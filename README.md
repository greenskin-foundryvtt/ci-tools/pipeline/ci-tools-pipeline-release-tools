# ci-tools-pipeline-release-tools

This module contains abstract gitlab ci function for release tools project

## release-tools.yml

Since 1.0.0

Abstract ci function to manage release for tools project (project with single project.json with version)

- update version of project.json
- push tag on git
- publish release on gitlab (only source, no artifact)

### Dependencies

- release-common
- a project.json file with version

```
{
  "version": "1.0.0"
}
```

### Usage

- declare 2 stages in this order : prepare_release and release
- create 3 manual job and extends the corresponding release type from release-tools

Warning : only one of thoses 3 jobs must be run for a single pipeline

```
include:
  - project: '$CI_PROJECT_ROOT_NAMESPACE/ci-tools/pipeline/ci-tools-pipeline-release'
    ref: 1.0.0
    file: '/release-common.yml'
  - project: '$CI_PROJECT_ROOT_NAMESPACE/ci-tools/pipeline/ci-tools-pipeline-release-tools'
    ref: 1.0.0
    file: '/release-tools.yml'

stages:
  - prepare_release
  - release
  
release:patch:
  extends: .release-tools:patch
  when: manual

release:minor:
  extends: .release-tools:minor
  when: manual

release:major:
  extends: .release-tools:major
  when: manual
```
